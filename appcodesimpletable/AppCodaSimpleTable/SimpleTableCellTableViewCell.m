//
//  SimpleTableCellTableViewCell.m
//  AppCodaSimpleTable
//
//  Created by Sulaiman Khan on 10/16/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import "SimpleTableCellTableViewCell.h"

@implementation SimpleTableCellTableViewCell


@synthesize nameLabel;
@synthesize thumbnailImageView;
@synthesize prepTimeLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
