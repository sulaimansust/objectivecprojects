//
//  ViewController.m
//  AppCodaLocationProject
//
//  Created by Sulaiman Khan on 10/21/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    CLLocationManager *locationManager;
    CLGeocoder *geoCoder;
}

@synthesize latitudeLabel;
@synthesize longitudeLabel;
@synthesize addressLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad is called noe");

    locationManager = [[CLLocationManager alloc]init];
    geoCoder = [[CLGeocoder alloc] init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Failed to get location" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:alertAction];
    [alert presentViewController:alert animated:YES completion:nil];
}


- (IBAction)getCurrentLocation:(id)sender {
    NSLog(@"getCurrentLocation button is clicked now");
    locationManager.delegate=self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //[CLLocationManager authorizationStatus];
    //[locationManager requestWhenInUseAuthorization];
    NSLog(@" isEnabled : %d",[CLLocationManager authorizationStatus]);
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError %@",error);
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    NSLog(@"didUpdateLocation %ld",(unsigned long)locations.count);
    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation!=nil) {
        longitudeLabel.text = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];
        latitudeLabel.text = [NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];

    }
    
    NSLog(@"Resolving the address");
    [locationManager stopUpdatingLocation];
    
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        
        if (error==nil && [placemarks count]>0) {
            CLPlacemark *placemark = [placemarks lastObject];
            addressLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                                 placemark.subThoroughfare, placemark.thoroughfare,
                                 placemark.postalCode, placemark.locality,
                                 placemark.administrativeArea,
                                 placemark.country];
        } else {
            NSLog(@"%@",error.debugDescription);
        }
        
        ;}];
    
}


@end
