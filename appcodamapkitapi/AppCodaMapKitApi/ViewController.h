//
//  ViewController.h
//  AppCodaMapKitApi
//
//  Created by Sulaiman Khan on 10/22/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

