//
//  ViewController.h
//  AppCodaSimpleTable
//
//  Created by Sulaiman Khan on 10/16/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@end

