//
//  ViewController.h
//  AppCodaSocialSharing
//
//  Created by Sulaiman Khan on 10/22/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)postToTwitter:(id)sender;

- (IBAction)postToFacebook:(id)sender;

@end

