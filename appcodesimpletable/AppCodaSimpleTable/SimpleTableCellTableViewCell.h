//
//  SimpleTableCellTableViewCell.h
//  AppCodaSimpleTable
//
//  Created by Sulaiman Khan on 10/16/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCellTableViewCell : UITableViewCell

@property(nonatomic, weak)  IBOutlet UILabel *nameLabel;
@property(nonatomic, weak) IBOutlet UILabel *prepTimeLabel;
@property(nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;

@end
