//
//  ViewController.m
//  AppCodaSimpleTable
//
//  Created by Sulaiman Khan on 10/16/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import "ViewController.h"
#import "SimpleTableCellTableViewCell.h"

@interface ViewController ()

@end

@implementation ViewController{
    NSMutableArray *tableData;
    NSMutableArray *thumbnails;
    NSMutableArray *prepTime;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //This portion of code load data from plist files
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"recepies" ofType:@"plist"];
    
    //NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    //tableData = [dict objectForKey:@"RecipeName"];
    //thumbnails = [dict objectForKey:@"Thumbnail"];
    //prepTime = [dict objectForKey:@"PrepTime"];

    tableData = [NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",nil];
    thumbnails = [NSMutableArray arrayWithObjects:@"1.png",@"2.jpg",@"3.jpg",@"1.png",@"2.jpg",@"3.jpg",@"1.png",@"2.jpg",@"3.jpg",@"1.png",nil];
    prepTime = [NSMutableArray arrayWithObjects:@"30 min", @"30 min", @"20 min", @"30 min", @"10 min", @"1 hour", @"45 min", @"5 min", @"30 min", @"8 min", @"20 min", @"20 min", @"5 min", @"1.5 hour", @"4 hours", @"10 min", nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{


    static NSString *simpleTableIdentifier = @"SimpleTableItem";

    SimpleTableCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.nameLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
    cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    
    
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    return  cell;


}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 78;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"MY Alert" message:[NSString stringWithFormat:@"%@ is selected ",[tableData objectAtIndex:indexPath.row]] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableData removeObjectAtIndex:indexPath.row];
    [tableView reloadData];
}

@end
