//
//  AppDelegate.h
//  AppCodaLocationProject
//
//  Created by Sulaiman Khan on 10/21/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

