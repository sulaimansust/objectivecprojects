//
//  ViewController.m
//  AppCodaSocialSharing
//
//  Created by Sulaiman Khan on 10/22/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import "ViewController.h"
#import <Social/Social.h>

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    // Do any additional setup after loading the view, typically from a nib.
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)postToTwitter:(id)sender {
    NSLog(@"postToTwitter button is clicked now");
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Great fun to learn iOS programming "];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    
}

- (IBAction)postToFacebook:(id)sender {
    NSLog(@"postToFacebook button is clicked now ");
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:@"First post from my iPhone app"];
        
        
        [controller addURL:[NSURL URLWithString:@"http://www.appcoda.com"]];
     //   [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image.jpg"]];
        
        
        [self presentViewController:controller animated:YES completion:Nil];
    }
    
}
@end
