//
//  ViewController.h
//  AppCodaSendEmail
//
//  Created by Sulaiman Khan on 10/21/17.
//  Copyright © 2017 IPVision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ViewController : UIViewController<MFMailComposeViewControllerDelegate>

- (IBAction)showEmail:(id)sender;

@end

